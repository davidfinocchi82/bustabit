var config = {
      baseBet: { value: 2, type: 'balance', label: 'Base Bet' },
      name: { value: 'C0deAssass1n', type: 'text', label: 'What is your name?' },
      baseMultiplier: { value: 2.03, type: 'multiplier', label: 'Base Multiplier' },
      betMultiplier: { value: 1.67, type: 'multiplier', label: 'Bet Multiplier' },
      streakSecurity: { value: 12, type: 'multiplier', label: 'Streak Security' },
      maximumBet: { value: 9999999999, type: 'multiplier', label: 'Maximum Bet' },
      pauseThreshold: { value: 20, type: 'multiplier', label: 'Pause Threshold' },
      continueThreshold: { value: 2, type: 'multiplier', label: 'Continue Threshold' },
      pauseAfterNLosses: { value: 99, type: 'multiplier', label: 'Pause After N Losses' },
      pauseForMGames: { value: 2, type: 'multiplier', label: 'Pause For M Games' },
      betPercentage: { value: 0.0001875, type: 'multiplier', label: 'Bet Percentage Of Bankroll' }
};
//var baseBet = 10; // In bits
//var baseMultiplier = 1.33; // Target multiplier: 1.33 recommended
//var variableBase = false; // Enable variable mode (very experimental), read streakSecurity.
//var streakSecurity = 3; // Number of loss-streak you wanna be safe for. Increasing this massively reduces the variableBase calculated. (1-loss = 30%, 3-loss = 5%, 3-loss = 1.35% of your maximum balance). Recommended: 3+
//var maximumBet = 999999999; // Maximum bet the bot will do (in bits).

// Pause settings
//var pauseThreshold = 20;  // when game crashes above this, betting pauses
//var continueThreshold = 2; // when paused and the game crashes above this, betting resumes

//var pauseAfterNLosses = 3;   
//var pauseForMGames = 7;

/******************/

// Variables - Do not touch!
var compound = true;
var variableBase = false;
var baseSatoshi = getBaseBet();
var currentBet = baseSatoshi;
var currentMultiplier = config.baseMultiplier.value;
var lossStreak = 0;
var coolingDown = false;

var lostLast = false;

// Pause Variables
var currentGameData;
var lastCrash = (config.continueThreshold.value + config.pauseThreshold.value)/3;
var paused = false;
var pauseLossStreak = 0;
var pausedFor = 0;


// Initialization
log('====== BustaBit Bot ======');
log('Starting balance: ' + (userInfo.balance / 100).toFixed(3) + ' bits');
xLog('====== BustaBit Bot ======');
xLog('Starting balance: ' + (userInfo.balance / 100).toFixed(3) + ' bits');
var startingBalance = userInfo.balance;

if (variableBase) {
      log('[WARN] Variable mode is enabled and not fully tested. Bot is resillient to ' + config.streakSecurity.value + '-loss streaks.');
      xLog('[WARN] Variable mode is enabled and not fully tested. Bot is resillient to ' + config.streakSecurity.value + '-loss streaks.');
}

// On a game starting, place the bet.
engine.on('GAME_STARTING', onGameStarted);
engine.on('GAME_ENDED', onGameEnded);
engine.on('GAME_STARTED', function(data) {
      log('[Bot] Game #' + data.gameID + ' has started!');
      xLog('[Bot] Game #' + data.gameID + ' has started!');
      currentGameData = data;
});

engine.on('CASHED_OUT', function(data) {
      if (config.name.value == data.uname) {
            log('[Bot] Successfully cashed out at ' + (data.cashedAt) + 'x');
            xLog('[Bot] Successfully cashed out at ' + (data.cashedAt) + 'x');
      }
});

function onGameStarted() {
      log('====== New Game ======');
      //xLog('====== New Game ======');

    /********************/
  
  if(lastCrash >= config.pauseThreshold.value) {
    paused = true;
    log("Pausing Betting");
    xLog("Pausing Betting");
    return;
  }
  
  if(paused) {
    if(lastCrash >= config.continueThreshold.value) {
      log("Continuing Betting");
      xLog("Continuing Betting");
      lastCrash = (config.continueThreshold.value + config.pauseThreshold.value)/3;
      paused = false;
    } else {
      log("Betting Is Paused");
      xLog("Betting Is Paused");
      return;
    }
  }

  /********************/
  
  if(pausedFor > 0) {
     pausedFor++;
     if(pausedFor <= config.pauseForMGames.value) {
       log("Paused " + pausedFor + " of " + config.pauseForMGames.value + " games");
       xLog("Paused " + pausedFor + " of " + config.pauseForMGames.value + " games");
       return;
     } else {
       log("Resuming");
       xLog("Resuming");
       pausedFor = 0;
       pauseLossStreak = 0;
     }
  } 
  
  if(pauseLossStreak >= config.pauseAfterNLosses.value) {
    log("Pausing for 1 of " + config.pauseForMGames.value + " games");
    xLog("Pausing for 1 of " + config.pauseForMGames.value + " games");
    pausedFor = 1;
    return;
  }

  /********************/

      if (coolingDown) {     
            if (lossStreak == 0) {
                  coolingDown = false;
            }
            else {
                  lossStreak--;
                  log('[Bot] Cooling down! Games remaining: ' + lossStreak);
                  xLog('[Bot] Cooling down! Games remaining: ' + lossStreak);
                  return;
            }
      }
      log('[Stats] Session profit: ' + ((userInfo.balance - startingBalance) / 100).toFixed(3) + ' bits');
      log('[Stats] Profit percentage: ' + (((userInfo.balance / startingBalance) - 1) * 100).toFixed(3) + '%');
      xLog('[Stats] Session profit: ' + ((userInfo.balance - startingBalance) / 100).toFixed(3) + ' bits');
      xLog('[Stats] Profit percentage: ' + (((userInfo.balance / startingBalance) - 1) * 100).toFixed(3) + '%');

      if (lostLast) {//if (engine.lastGamePlay() == 'LOST') { // If last game loss:
            lossStreak++;
            var totalLosses = 0; // Total satoshi lost.
            var lastLoss = currentBet; // Store our last bet.
            while (lastLoss >= baseSatoshi) { // Until we get down to base bet, add the previous losses.
                  totalLosses += lastLoss;
                  lastLoss /= 3;
            }

            if (lossStreak >= config.streakSecurity.value) { // If we're on a loss streak, wait a few games!
                  coolingDown = true;
                  lostLast = false;
                  xLog("Taken " + lossStreak + " losses in a row.", true);
                  return;
            }
            var newBet = currentBet * config.betMultiplier.value;
            currentBet = Math.ceil(newBet); // Then multiply base bet by 3!
            var nextMultiplier = Math.floor(config.baseMultiplier.value) + (totalLosses / currentBet);
            currentMultiplier = Number(nextMultiplier.toFixed(2));
            placeBet();
      }
      else { // Otherwise if win or first game:
            lossStreak = 0; // If it was a win, we reset the lossStreak.
            if (variableBase) { // If variable bet enabled.
                  // Variable mode resists (currently) 1 loss, by making sure you have enough to cover the base and the 3x base bet.
                  var divider = 100;
                  for (i = 0; i < config.streakSecurity.value; i++) {
                        divider += (100 * Math.pow(3, (i + 1)));
                  }

                  newBaseBet = Math.min(Math.max(1, Math.floor(userInfo.balance / divider)), config.maximumBet.value * 100); // In bits
                  newBaseSatoshi = newBaseBet * 100;

                  if ((newBaseBet != config.baseBet.value) || (newBaseBet == 1)) {
                        log('[Bot] Variable mode has changed base bet to: ' + newBaseBet + ' bits');
                        xLog('[Bot] Variable mode has changed base bet to: ' + newBaseBet + ' bits');
                        config.baseBet.value = newBaseBet;
                        baseSatoshi = newBaseSatoshi;
                  }
            }
            // Update bet.
            baseSatoshi = getBaseBet();
            currentBet = baseSatoshi; // in Satoshi
            currentMultiplier = config.baseMultiplier.value;
            placeBet();
      }
};

function onGameEnded() {
      var lastGame = engine.history.first();

      lastCrash = lastGame.bust;

      if (!lastGame.wager) {
            return;
      }

      log('[Bot] Game Busted at ' + (lastGame.bust) + 'x');
      xLog('Total Balance: ' + (userInfo.balance / 100).toFixed(3) + ' bits');

      /********************/

      if (lastGame.cashedAt) {
            pauseLossStreak = 0;
            log("Won this game.");
            xLog("Won this game.\n-\n");
            lostLast = false;
      } else {
            pauseLossStreak++;
            log("Lost this game.");
            xLog("Lost this game.\n-\n");
            lostLast = true;
      }

      /********************/
};

function placeBet() {
      // Message and set first game to false to be sure.
      log('[Bot] Betting ' + (currentBet / 100) + ' bits, cashing out at ' + currentMultiplier + 'x');
      xLog('[Bot] Betting ' + (currentBet / 100) + ' bits, cashing out at ' + currentMultiplier + 'x');

      if (currentBet <= userInfo.balance) { // Ensure we have enough to bet
            if (currentBet > (config.maximumBet.value * 100)) { // Ensure you only bet the maximum.
                  currentBet = config.maximumBet.value;
            }
            engine.bet(roundBit(currentBet), currentMultiplier);
      }
      else { // Otherwise insufficent funds...
            if (userInfo.balance < 100) {
                  log('[Bot] Insufficent funds to do anything... stopping');
                  xLog('[Bot] Insufficent funds to do anything... stopping');
                  engine.stop();
            }
            else {
                  log('[Bot] Insufficent funds to bet ' + (currentBet / 100) + ' bits.');
                  log('[Bot] Resetting to 1 bit basebet');
                  xLog('[Bot] Insufficent funds to bet ' + (currentBet / 100) + ' bits.', true);
                  xLog('[Bot] Resetting to 1 bit basebet');
                  lostLast = false;
            }
      }
};

function getBaseBet() {
  if (compound) {
    return roundBit(userInfo.balance * config.betPercentage.value);
  } else {
    return config.baseBet.value * 100;
  }
};

function roundBit(bet) {
  return Math.round(bet / 100) * 100;
};




// logs to a private discord channel
// set `isAlert` to true to send @here to channel
function xLog(message, isAlert) {
  isAlert = isAlert || false;

  var msg = message;
  if(isAlert == true)
    msg = "@here \n" + message;

  var xhr = new XMLHttpRequest();
  var url = "https://discordapp.com/api/webhooks/489875749982175232/ZFN58Hb-OqRQTZ3bJIkljay5UiyizLSQqVyYE8F8LUm7e52_P9q0B39ZHCd7T5EQxofG";
  xhr.open("POST", url, true);
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200) {
          var json = JSON.parse(xhr.responseText);
      }
  }
  var data = JSON.stringify({
    "content": msg
  });
  xhr.send(data);
}